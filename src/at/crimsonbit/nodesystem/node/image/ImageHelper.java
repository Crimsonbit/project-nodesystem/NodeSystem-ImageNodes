package at.crimsonbit.nodesystem.node.image;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum ImageHelper implements IGuiNodeType {

	IMAGE_HEIGHT("Image-Height Node"), IMAGE_WIDTH("Image-Width Node"), IMAGE_COMP_SIZE("Image-Compare-Size Node");

	private String name;

	private ImageHelper(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return GNode.class;
	}

	@Override
	public Color getColor() {
		return Color.SADDLEBROWN;
	}
}

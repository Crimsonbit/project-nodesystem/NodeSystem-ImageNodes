package at.crimsonbit.nodesystem.node.image;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum Image implements IGuiNodeType{
	IMAGE_EMPTY("Empty Image Node"), IMAGE_LOADER("Image-Loader Node"), IMAGE_SAVER("Image-Saver Node"), IMAGE_DISPLAY(
			"Image-Display Node");

	private String name;

	private Image(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return ImageNodeClass.class;
	}

	@Override
	public Color getColor() {
		return Color.BROWN;
	}
}
